# README #

This repository is hosting unofficial builds of the hydrogen drum machine for Mac OS X Yosemite and El Capitan.

I am not the original author nor am I associated with hydrogen development. 
You can get the source code as well as documentation from [hydrogen-music.org](http://www.hydrogen-music.org). 
The official hydrogen repository can be found [here](https://github.com/hydrogen-music/hydrogen)

### How do I get set up? ###
* download [disk image](https://bitbucket.org/andrejanowicz/hydrogen-build/downloads/hydrogen_0.9.7.dmg)
* mount, copy contents to /Applications/, [allow running programs from an unidentified developer](http://stackoverflow.com/questions/19551298/app-cant-be-opened-because-it-is-from-an-unidentified-developer), done.